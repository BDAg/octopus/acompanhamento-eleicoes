from tweepy import OAuthHandler
from tweepy import API
from datetime import datetime
from datetime import timedelta, timezone
import pymongo
import time
import twitter_api_credentials

def enviaDados(datahora, candidato, curtidas):
    mydb.monitoramentoSeguidoresTwitter.update  (         
                                                    {
                                                        "candidato" : candidato
                                                    },
                                                    {
                                                        "candidato" : candidato,
                                                        "curtidas" : curtidas,
                                                        "DateTime" : datahora
                                                    },
													upsert=True,
													check_keys=True
                                                )

    mydb.historicoMonitoramentoSeguidoresTwitter.update (
                                                            {
                                                                "DateTime" : datahora,
                                                                "candidato" : candidato
                                                            },
                                                            {
                                                                "candidato" : candidato,
                                                                "curtidas" : curtidas,
                                                                "DateTime" : datahora
                                                            },
                                                            upsert=True,
                                                            check_keys=True
                                                        )


auth = OAuthHandler(twitter_api_credentials.CONSUMER_KEY, twitter_api_credentials.CONSUMER_SECRET)
auth.set_access_token(twitter_api_credentials.ACCESS_TOKEN, twitter_api_credentials.ACCESS_TOKEN_SECRET)
api = API(auth)

targets = ['jairbolsonaro', 'geraldoalckmin', 'Haddad_Fernando', 'joaoamoedonovo', 'MarinaSilva', 'cirogomes'] # All your targets here


while True:

    try:
        conexao = pymongo.MongoClient("mongodb+srv://bottwitter:Daciolo2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")
        mydb = conexao['eleicoes']

        try:
            for target in targets:
                user = api.get_user(target)
                candidato = user.screen_name
                curtidas = user.followers_count
                diferencaHorario = timedelta(hours = -3)
                fusoHorario = timezone(diferencaHorario)
                datahora = datetime.now().astimezone(fusoHorario)
                enviaDados(datahora, candidato, curtidas)
                print(datahora, candidato, curtidas)
        
        except:
            pass

        finally:            
            conexao.close()

    except:
        pass
