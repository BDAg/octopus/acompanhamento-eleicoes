'''

	CUIDADO! SCRIPT COM POTENCIAL DESTRUTIVO, CASO SEU DESEJO SEJA RODÁ-LO PARA SEMPRE, SIGA EM FRENTE, OBRIGADO!

'''

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy import API
from tweepy import RateLimitError
from tweepy import Cursor

import twitter_api_credentials
import json
import pymongo
from datetime import datetime
from datetime import timedelta, timezone

from urllib3.exceptions import ProtocolError

try:
	conexao = pymongo.MongoClient("mongodb+srv://bottwitter:Daciolo2018@octopus-4fjdp.mongodb.net/test?retryWrites=true")
	mydb = conexao['eleicoes']
except:
	print("Erro ao conectar com o MongoDB")

auth = OAuthHandler(twitter_api_credentials.CONSUMER_KEY, twitter_api_credentials.CONSUMER_SECRET)
auth.set_access_token(twitter_api_credentials.ACCESS_TOKEN, twitter_api_credentials.ACCESS_TOKEN_SECRET)
api = API(auth)

diferencaHorario = timedelta(hours = -3)
fusoHorario = timezone(diferencaHorario)

def enviaDados(tweet, data, nomePessoa):
	mydb.comentarios.update(
								{
									"comentario" 	: tweet,
									"data" 			: data
								},
								{
									"comentario" 	: tweet,
									"data" 			: data,
									"tipo"			: "twitter",
									"postou"		: nomePessoa
								},
								upsert=True
							)

class TwitterStreamer():
	def stream_tweets(self, hash_tag_list):
		try:
			listener = StdOutListener()
			stream = Stream(auth=api.auth, listener=listener, tweet_mode='extended')
			stream.filter(languages=["pt"], track=hash_tag_list)
		except:
			print("Erro ao conectar com o Twitter")

class StdOutListener(StreamListener):
	def on_data(self, data):
		tweet = json.loads(data)
		DataHoraCaptura = datetime.now().astimezone(fusoHorario)
		if 'extended_tweet' in tweet:
		   print('name: '+tweet['user']['screen_name']+' -> '+tweet['extended_tweet']['full_text'])
		   nome = tweet['user']['screen_name']
		   tweet = tweet['extended_tweet']['full_text']
		   enviaDados(tweet, DataHoraCaptura, nome)
		elif 'retweeted_status' in tweet:
			retweet = tweet['retweeted_status']
			if 'extended_tweet' in retweet:
			   print('name: '+tweet['user']['screen_name']+' -> '+tweet['retweeted_status']['extended_tweet']['full_text'])
			   nome = tweet['user']['screen_name']
			   tweet = tweet['retweeted_status']['extended_tweet']['full_text']
			   enviaDados(tweet, DataHoraCaptura, nome)
			else:
			   print('name: '+tweet['user']['screen_name']+' -> '+tweet['retweeted_status']['text'])
			   nome = tweet['user']['screen_name']
			   tweet = tweet['retweeted_status']['text']
			   enviaDados(tweet, DataHoraCaptura, nome)
		elif 'text' in tweet:
		   print('name: '+tweet['user']['screen_name']+' -> '+tweet['text'])
		   nome = tweet['user']['screen_name']
		   tweet = tweet['text']
		   enviaDados(tweet, DataHoraCaptura, nome)

	def on_error(self, status):
		print("Erro:", status)

while True:

	twitter_streamer = TwitterStreamer()

	filtros = ['jairbolsonaro', 'MarinaSilva', 'Haddad_Fernando', 'cirogomes', 'joaoamoedonovo', '#elenão', '#EleSim', '#ApoieHaddadManu', '#BolsonaroNãoÉCorrupto', '#Geraldo45', '#VemComJoão30', '#Bolsomito', '#Bolsonaro17', '#Ciro12', '#Vote13', '#DiaDeMarina', '#EleSim', '#politica', '#LulaLivre', 'Bolsonaro', 'Haddad', 'Ciro', 'Alckmin', 'Marina Silva', 'Amoedo', 'Politica', 'PDT', 'Davila', 'Lula', 'PT', 'Katia Abreu', 'Jair se acostumando', 'Bolsomito', 'Partido Novo', 'PSL', 'PSDB', 'Rede 18', 'Politica', 'Politicos', 'Eleições', 'Presidente', 'voto', 'Candidato', 'Partido', 'Vice-presidente']
	twitter_streamer.stream_tweets(filtros)
