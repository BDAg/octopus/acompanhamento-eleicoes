var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var monitoramentoInstagram = new Schema({
    candidato: { type: String },
    seguidores: { type: Number },
    datetime: { type: Date },
    }, {
    collection: "monitoramentoSeguidoresInstagram"
    })

module.exports = mongoose.model("MonitoramentoPaginasInstagram", monitoramentoInstagram);